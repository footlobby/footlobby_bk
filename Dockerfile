FROM python:3.8.10-slim-buster

WORKDIR /app

COPY . /app

RUN pip3 install --no-cache-dir -r requirements.txt

EXPOSE 8000

CMD ["gunicorn", "run:app", "-w", "4", "-b", "0.0.0.0"]