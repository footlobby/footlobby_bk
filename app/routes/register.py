from flask import Blueprint, jsonify, redirect, url_for
from app.models.models import User
from flask_login import current_user
from app.models.forms import RegistrationForm
from app import db
from app.tools.password import hashPassword
from app.tools.checkPasswordComplexity import is_strong_password

registeration = Blueprint('signup', __name__)

@registeration.route('/register', methods=['POST', 'GET'])
def register():
    if current_user.is_authenticated:
        message = "You must logout first"
        return jsonify({"error": message}), 400
    
    form = RegistrationForm()

    if form.validate_on_submit():
        userExists = User.query.filter_by(username=form.username.data).first()
        emailExists = User.query.filter_by(email=form.email.data).first()

        if userExists:
            return jsonify({"error": {"Username": ["Username already taken"]}}), 400
        if emailExists:
            return jsonify({"error": {"E-mail": ["E-mail used on an other account"]}}), 400
        
        clearPassword = form.password.data
        
        if not is_strong_password(clearPassword):
            return jsonify({"error": {"Password": ["Password is not strong enough"]}}), 400
        
        hashedPassword = hashPassword(clearPassword)

        newUser = User(
            username=form.username.data,
            email=form.email.data,
            password=hashedPassword,
            first_name=form.firstName.data,
            last_name=form.lastName.data,
            birthday=form.birthday.data,
            phone_number=form.phone.data
        )

        try:
            db.session.add(newUser)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            return jsonify({"error": "An error occurred while registering"}), 500
        
        CheckNewUser = User.query.filter_by(username=newUser.username)
        if CheckNewUser:
            message = "Registered successfully"
        return jsonify({"message": message})
    
    return jsonify({"error": form.errors}), 400

