from flask import Blueprint, jsonify, request, make_response
from flask_login import login_user, logout_user, current_user, login_required
from app.models.forms import LoginForm
from app.models.models import User
from app.tools.password import checkPassword
import jwt
import datetime
import os
from app import redis
from redis.exceptions import RedisError

from functools import wraps

auth = Blueprint('auth', __name__)

@auth.route('/login', methods=['POST', 'GET'])
def login():
    if current_user.is_authenticated:
        message = "Already logged in"
        return jsonify({"message": message})

    if request.method == 'POST':
        form = LoginForm()

        if form.validate_on_submit():
            username = User.query.filter_by(username=form.username.data).first()
            if username:
                clearPassword = form.password.data
                hashedPassword = username.password
                if checkPassword(clearPassword, hashedPassword):
                    login_user(username)
                    user_id = username.id

                    # Create JWT Token
                    payload = {
                        'sub': int(user_id),
                        'iat': datetime.datetime.utcnow(),
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=120)
                    }
                    secret_key = os.getenv('SECRET_KEY')
                    token = jwt.encode(payload, secret_key, algorithm='HS256')
                    try:
                        redis.set(f'jwt-user_id-{user_id}', token)
                    except RedisError as e:
                        print(f"Redis Error: {e}")
                    except Exception as e:
                        print(f"An unexpected error occurred: {e}")
  
                    response = make_response("Logged in successfully.")
                    response.set_cookie('jwt_token', token, httponly=True, secure=False, expires=(datetime.datetime.now() + datetime.timedelta(minutes=120)))

                    return response
                else:
                    message = "Incorrect login or password"
                    return jsonify({"error": message}), 400  
            else:
                message = "User not found"
                return jsonify({"error": message}), 404
        return jsonify({"error": form.errors}), 400

@auth.route('/logout', methods=['GET'])
@login_required
def logout():
    # Clear cookie
    response = make_response("User logged out")
    response.set_cookie('jwt_token', '', expires=0)
    
    # Clear redis key
    user_id = current_user.id
    try:
        redis_key = f'jwt-user_id-{user_id}'
        redis.delete(redis_key)
    except RedisError as e:
        print(f"Redis Error: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")
    
    logout_user()

    return response