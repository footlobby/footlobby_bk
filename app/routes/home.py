from flask import Blueprint, jsonify
from flask_login import current_user

dashboard = Blueprint('dashboard', __name__)

@dashboard.route('/home', methods=['GET', 'POST'])
def home():
    if current_user.is_authenticated:
        username = current_user.username
        message = f"You are at the home page, {username}"
        return jsonify({"message": message}), 200
    else:
        return jsonify({"message": "You are not logged in."}), 401