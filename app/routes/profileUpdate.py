from flask import Blueprint, request, jsonify, make_response
from flask_login import login_required, current_user
from app.models.models import User
from app.models.forms import UpdateForm
import jwt
import os
from app import db
from app.tools.password import hashPassword

profileUpdate = Blueprint('Profile_Update', __name__)

@profileUpdate.route('/g_profile', methods=['GET'])
@login_required
def getProfile():
    user_id = current_user.id
    user = User.query.get(user_id)

    if user:
        userInfo = {
                'username': user.username,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'birthday': user.birthday,
                'email': user.email,
                'phone_number': user.phone_number
        }
        return jsonify(userInfo), 200
    else:
        return jsonify({'message': 'User not found'}), 400

@profileUpdate.route('/p_update', methods=['PUT'])
@login_required
def p_update():
    user_id = current_user.id
    
    # Check if the JWT token is present in the request's cookies
    jwt_token = request.cookies.get('jwt_token')
    
    if not jwt_token:
        return jsonify({'message': 'Unauthorized'}), 401  # 401 Unauthorized

    try:
        secret_key = os.getenv('SECRET_KEY')
        payload = jwt.decode(jwt_token, secret_key, algorithms=['HS256'])

        if user_id != payload['sub']:
            return jsonify({'message': 'You are not authorized to update this profile'}), 403  # 403 Forbidden

        if request.method == 'PUT':
            form = UpdateForm()
    
            if form.validate_on_submit():
                user = User.query.get(user_id)
                print(user_id)

                if form.password.data:
                    clearPassword = form.password.data
                    hashedPassword = hashPassword(clearPassword)
                    user.password = hashedPassword

                if form.firstName.data:
                    user.first_name = form.firstName.data
                
                if form.lastName.data:
                    user.last_name = form.lastName.data
                
                if form.birthday.data:
                    user.birthday = form.birthday.data
                
                if form.phone.data:
                    user.phone_number = form.phone.data
                
    
                db.session.commit()
    
                return jsonify({'message': 'Profile updated successfully'}), 200  # 200 OK

    except jwt.ExpiredSignatureError:
        return jsonify({'message': 'JWT token has expired'}), 401  # 401 Unauthorized
    except jwt.InvalidTokenError:
        return jsonify({'message': 'Invalid JWT token'}), 401  # 401 Unauthorized

    # Return an error response if the data doesn't contain valid updates
    return jsonify({'message': 'Invalid data or user not found'}), 400  # 400 Bad Request
