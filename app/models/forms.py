from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, DateField, SubmitField, TelField
from wtforms.validators import DataRequired, Email, Length, EqualTo, Optional

class RegistrationForm(FlaskForm):
    username = StringField('Username',validators=[DataRequired(), Length(min=4, max=24)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired(), Length(min=8)])
    confirmPassword = PasswordField('Confirm Password', validators=[DataRequired(), Length(min=8)])
    firstName = StringField('First Name', validators=[DataRequired(), Length(max=50)])
    lastName = StringField('Last Name', validators=[DataRequired(), Length(max=50)])
    birthday = DateField('Birthday', format='%d-%m-%Y')
    phone = TelField('Phone')
    submit = SubmitField('Register')

class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField('Register')

class UpdateForm(FlaskForm):
    email = StringField('Email', validators=[Optional(), Email()])
    password = PasswordField('Password', validators=[Optional(), Length(min=8)])
    confirmPassword = PasswordField('Confirm Password', validators=[Optional(), Length(min=8), EqualTo('password', message='Passwords must match')])
    firstName = StringField('First Name', validators=[Optional(), Length(max=50)])
    lastName = StringField('Last Name', validators=[Optional(), Length(max=50)])
    birthday = DateField('Birthday', format='%d-%m-%Y', validators=[Optional()])
    phone = TelField('Phone', validators=[Optional()])
    submit = SubmitField('Update')