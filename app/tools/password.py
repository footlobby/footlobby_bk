from flask_bcrypt import Bcrypt

bcrypt = Bcrypt()

def hashPassword(password):
    return bcrypt.generate_password_hash(password).decode('utf-8')

def checkPassword(password, hashedPassword):
    return bcrypt.check_password_hash(hashedPassword, password)
