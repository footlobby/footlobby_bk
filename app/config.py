import os
from dotenv import load_dotenv

load_dotenv()

db_name = os.getenv('DB_NAME')
db_host = os.getenv('DB_HOST')
db_username = os.getenv('DB_USERNAME')
db_password = os.getenv('DB_PASSWORD')
db_port = os.getenv('DB_PORT')
redis_host = os.getenv('REDIS_HOST')
redis_port = os.getenv('REDIS_PORT')
redis_index = os.getenv('REDIS_INDEX')
redis_pwd = os.getenv('REDIS_PWD')

SECRET_KEY = os.getenv('SECRET_KEY')

WTF_CSRF_ENABLED = False

SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{db_username}:{db_password}@{db_host}:{db_port}/{db_name}'
SQLALCHEMY_TRACK_MODIFICATIONS = False

REDIS_URL = f'redis://:{redis_pwd}@{redis_host}:{redis_port}/{redis_index}'
SESSION_TYPE = 'redis'
SESSION_PERMANENT = False
SESSION_USE_SIGNER = True
SESSION_KEY_PREFIX = 'ftlb_session:'
SESSION_REDIS = {
    'host': redis_host,
    'port': redis_port,
    'password': redis_pwd,
    'db': redis_index
}
